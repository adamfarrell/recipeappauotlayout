//
//  UIColor+CustomColors.h
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+(UIColor*)primaryBackgroundColor;
+(UIColor*)primaryButtonFontColor;
+(UIColor*)primaryButtonColor;
+(UIColor*)primaryFontColor;

@end
