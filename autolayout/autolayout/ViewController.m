//
//  ViewController.m
//  autolayout
//
//  Created by Adam Farrell on 6/16/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString* filePath = [[NSBundle mainBundle]pathForResource:@"RecipePropertyList" ofType:@"plist"];
    arrayOfRecipes = [[NSArray alloc]initWithContentsOfFile:filePath];
    
    UICollectionViewFlowLayout* recipeFlow = [[UICollectionViewFlowLayout alloc]init];
    
    UICollectionView* recipeList = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 0) collectionViewLayout:recipeFlow];
    recipeList.delegate = self;
    recipeList.dataSource = self;
    recipeList.backgroundColor = [UIColor primaryBackgroundColor];
    
    [recipeList registerClass:[ImageCVC class] forCellWithReuseIdentifier:@"cell"];
    
    [self.view addSubview:recipeList];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCVC* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary* temp = arrayOfRecipes[indexPath.row];
    cell.image.image = [UIImage imageNamed:[temp objectForKey:@"image"]];
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrayOfRecipes.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width / 2 - 20, 200);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(12, 12, 12, 12);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    NSDictionary* selectedDictionary = arrayOfRecipes[indexPath.row];
    RecipeViewController* recipe = [RecipeViewController new];
    recipe.recipeDictionary = selectedDictionary;
    [self.navigationController pushViewController:recipe animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
