//
//  UIColor+CustomColors.m
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+(UIColor*)primaryBackgroundColor {
    return [UIColor colorWithRed:199.0 / 255.0 green:232.0 / 255.0 blue:1.0 alpha:1.0];
}

+(UIColor*)primaryButtonFontColor {
    return [UIColor colorWithRed:250.0 / 255.0 green:250.0 / 255.0 blue:250.0 / 255.0 alpha:1.0];
}

+(UIColor*)primaryButtonColor {
    return [UIColor colorWithRed:17.0 / 255.0 green:71.0 / 255.0 blue:158.0 / 255.0 alpha:1.0];
}

+(UIColor*)primaryFontColor {
    return [UIColor colorWithRed:64.0 / 255.0 green:64.0 / 255.0 blue:64.0 / 255.0 alpha:1.0];
}

@end
