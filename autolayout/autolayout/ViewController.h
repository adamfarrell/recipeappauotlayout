//
//  ViewController.h
//  autolayout
//
//  Created by Adam Farrell on 6/16/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import "ImageCVC.h"
#import "RecipeViewController.h"

@interface ViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    NSArray* arrayOfRecipes;
}

@end

